from html.parser import HTMLParser
from html.entities import name2codepoint
import urllib.request
import urllib

from collections import deque, defaultdict
import sys
import re


def dq_inters(d1, d2):
    """ Check if d1 and d2 have some elements in common """
    for i in d1:
        if i in d2:
            return True
    return False


def dq_trunc(d, e):
    """ Remove elements from d, until it finds e, and remove it
        Don't anything if there is no element like e in d
    """

    if e in d:
        while d.pop() != e:
            pass


class MinifyHTML(HTMLParser, object):

    """generate a minified html"""

    def __init__(self):
        self.path = deque()
        self.blacklist = deque(('script',))
        self.tag1 = ('link', 'meta', 'p', 'img', 'br', 'hr')
        super(MinifyHTML, self).__init__()
        self.out = sys.stdout
        self.debug = False
        self.spaces = re.compile("  +")
        self.links = deque()  # list of url
        self.base = None
        self.convert_url = lambda x: x

    def set_output(self, out):
        """ Set the output file.
            out -- a file object
        """
        self.out = out

    def set_base(self, base):
        """ set base url, used for relative links """
        self.base = base

    def get_linkfound(self):
        """ Set a list to which append all the link found """
        return (self.base, self.links)

    def handle_starttag(self, tag, attrs):
        self._debug("Start tag:", tag)
        for attr in attrs:
            self._debug("     attr:", attr)
        if not tag in self.tag1:
            self.path.append(tag)
        if dq_inters(self.path, self.blacklist):
            # I'm inside a blacklisted tag
            return
        if len(attrs) == 0:
            self.out.write("<" + tag + ">")
        else:
            attributes = self.remap_attrs(tag, attrs)
            self.out.write("<" + tag + " " + " ".join(
                [a + "='" + v + "'" for a, v in attributes if a and v]) + ">")
            # TODO: why there are a or v = None?
        # self.out.write(self.get_starttag_text())
        if tag.lower() == "a":
            self.add_link(attrs)
        if tag.lower() == "base":
            self.add_base(attrs)

    def handle_endtag(self, tag):
        self._debug("End tag  :", tag)
        if not dq_inters(self.path, self.blacklist):
            # I'm inside a blacklisted tag
            self.out.write("</" + tag + ">")
        dq_trunc(self.path, tag)

    def handle_data(self, data):
        if not dq_inters(self.path, self.blacklist):
            self._debug("Path     :", self.path)
            self._debug("Data     :", data)
            data = self.spaces.sub(' ', data)
            self.out.write(data)

    def handle_comment(self, data):
        pass
        # self._debug( "Comment  :", data)

    def handle_entityref(self, name):
        c = chr(name2codepoint[name])
        self._debug("Named ent: &%s;" % name)
        self._debug("Named ent:", c.encode('utf-8'))
        self.out.write("&%s;" % name)

    def handle_charref(self, name):
        if name.startswith('x'):
            c = chr(int(name[1:], 16))
        else:
            c = chr(int(name))
        self._debug("Num ent  : &x%d;" % c)
        self._debug("Num ent  :", c)
        self.out.write("&x%d;")

    def handle_decl(self, data):
        self._debug("Decl     :", data)

    def set_url_converter(self, funct):
        self.convert_url = funct

    def convert_url_to_cache(self, url):
        """ return url itself, if is not cached """
        new_url = url
        try:
            new_url = self.convert_url(url)
        except Exception as e:
            print(e)
        return new_url

    def simple_url_converter(self, prefix):
        def _concat(url):
            new_url = url
            u = urllib.parse.urlparse(url)
            if u.scheme in ('http', 'https'):
                new_url = prefix + \
                    u.netloc + "/" + u.path
            return new_url

        return _concat

    def remap_attrs(self, tag, attrs):
        ad = defaultdict(lambda: None, attrs)
        if tag == 'a':
            href = ad['href']
            if 'onclick' in ad:
                del ad['onclick']  # remove javascript from link
            if href:
                self._debug("original href=" + repr(href))
                href = urllib.parse.urljoin(self.base, href)
                self._debug("base href=" + repr(href))
                href = self.convert_url_to_cache(href)
                self._debug("new href=" + repr(href))
            ad['href'] = href
        return ad.items()

    def add_link(self, attrs):
        # check if it is a relative url, and make it an absolute one
        attrs = dict(attrs)
        if "href" in attrs:
            target = attrs["href"]
            self.links.append(target)

    def add_base(self, attrs):
        attrs = dict(attrs)
        if "href" in attrs:
            target = attrs["href"]
            self.base = dict(attrs)["href"]

    def _debug(self, *s):
        if not self.debug:
            return
        print("DEBUG:", end=' ')
        for i in s:
            print(i, end=' ')
        print()

    def reset(self):
        self.links = deque()
        HTMLParser.reset(self)
