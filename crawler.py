import collections
from MinifyHTML import MinifyHTML

from urllib.parse import urlparse
import urllib
import os
import requests
import cachecontrol
import string

#
# crawler is a module to implement a web-crowler
#
# pages are downloaded and saved in "minimized form"

# at the core, there is a queue of pages to be downloaded
# future version can be multi(thread|process) safe


class DownloadRequest:

    def __init__(self, url, recurse=True, reclevel=1, filter=None, same_domain=False, ref=None):
        self.url = url
        self.reclevel = int(reclevel)
        self.filter = filter
        self.same_domain = same_domain
        self.ref = ref

    # TODO:  Would be better to use multiple setter?
    def parse(self):
        """ parse the url to be downloaded, and return a tuple with:
            url, scheme, host&port, context, and page name """
        u = urlparse(self.url)
        context = os.path.dirname(u.path)
        page = os.path.basename(u.path)
        if page == "":
            page = "index.html"

        return (self.url, u.scheme, u.netloc, context, page)

    def same_url(self, other):
        return self.url == other.url

    def __repr__(self):
        return "DownloadRequest(url='{0}', recurse={1}, reclevel={2}, filter={3}, same_domain={4} ref={5})"\
            "".format(self.url, (self.reclevel > 0), self.reclevel,
                      self.filter, self.same_domain, self.ref)


class DownloadQueue:

    def __init__(self):
        """ Create an empty queue """
        self.queue = collections.deque()
        self.visited = {}

    def already_there(self, req):
        for i in self.iterator():
            if i.same_url(req):
                return True
        return False

    def push(self, req):
        """ add a new DownloadRequest """
        if not self.already_there(req):
            self.queue.append(req)

    def pop(self):
        """ get the next DownloadRequest """
        return self.queue.popleft()

    def add_processed(self, req, result):
        self.visited[req] = result

    def is_processed(self, req):
        return req in self.visited

    def processed(self):
        return dict(self.visited)

    def is_empty(self):
        return len(self.queue) == 0

    def iterator(self):
        return iter(self.queue)


class WebCrawler:

    def __init__(self):
        self.dq = DownloadQueue()
        self.htmlFilter = MinifyHTML()
        self.sess = cachecontrol.CacheControl(requests.Session())
        self.url_converter = lambda x: x

    def configure(self, path=".", cache_url="http://localhost/cache/"):
        """ path: where downloaded resource should be stored """
        self.basedir = path[:]
        self.url_converter = self.htmlFilter.simple_url_converter(cache_url)
        self.htmlFilter.set_url_converter(self.url_converter)

    def is_empty(self):
        return self.dq.is_empty()

    def add(self, dr):
        """ add a DownloadRequest to the queue """
        self.dq.push(dr)

    def queue(self):
        """ return an iterator for the download queue """
        return self.dq.iterator()

    def process(self):
        """ process the next element in the queue
            return None, if nothing to process, otherwise the http status code
        """

        if self.is_empty():
            return None

        print("queue len:", len(self.dq.queue))

        req = self.dq.pop()
        (url, scheme, host, context, page) = req.parse()

        path = self.basedir + "/" + host + "/" + context
        filename = path + "/" + page

        self.htmlFilter.set_base(url)
        self.htmlFilter.reset()

        # create path if needed
        os.makedirs(path, exist_ok=True)

        print("Request url => %s (ref=%s)" % (url, req.ref))

        try:
            resp = self.sess.get(url, timeout=5)
        except requests.exceptions.ConnectionError as err:
            resp = None
            print("Error: ", err)
        except requests.packages.urllib3.exceptions.ReadTimeoutError as err:
            resp = None
            print("Error: ", err)
        except requests.exceptions.ReadTimeout as err:
            resp = None
            print("Error: ", err)

        if resp and resp.status_code < 400:
            print("Filename=", filename)
            # TODO: if the file is a directory save as "filename/index.html"
            if os.path.isdir(filename):
                filename = filename + "/index.html"
            with open(filename, "w") as f:
                print("-> Salvo in " + filename)
                # print("-> len(text)="+len(resp.text))
                self.htmlFilter.set_output(f)
                self.htmlFilter.feed(resp.text)

        self.dq.add_processed(url, resp)

        # TODO: get_linkfound should remove duplicated link
        # TODO: when I add a url to the queue I should check is not already
        # there or downloaded recently
        print("get_linkfound()")
        b, q = self.htmlFilter.get_linkfound()
        if (req.reclevel > 0):
            for u in self._fix_urls(b, q):
                # TODO: check if we need to add to the queue
                # if is_to_be_downloaded(u):
                u2 = urlparse(u)
                if u2.netloc.lower().find("google.com") > 0:
                    continue
                dr = DownloadRequest(u, reclevel=req.reclevel - 1, ref=url)
                if dr.parse()[1] in ('http', 'https'):
                    print("add url: %s" % (u,))
                    self.dq.push(dr)

        return url, resp

    def close(self):
        self.sess.close()
        del self.sess

    def _normalize_url(self, url):
        """ normalize the url """
        return urllib.parse.urlsplit(urllib.parse.urldefrag(url)[0]).geturl()

    def _fix_urls(self, base, list):
        """ normalize links, and make them absolute """
        newlist = collections.deque()
        for url in list:
            abs_url = urllib.parse.urljoin(base, url)
            nabs_url = self._normalize_url(abs_url)
            # print("===>
            # ","base=",base,"url=",url,"abs=",abs_url,"nabs=",nabs_url )
            newlist.append(nabs_url)
        return newlist


def main():
    print('crawler.main')
    spider = WebCrawler()
    spider.configure(
        path="/tmp/cache", cache_url="http://am1.soben.io/cache/")
#    spider.add(DownloadRequest("https://www.trueelena.org/", reclevel=2))
# spider.set_convert_url(
# MinifyHTML.simple_url_converter("http://am1.soben.io/cache/") )
    spider.add(DownloadRequest("https://lwn.net", reclevel=0))
    res = 0

    while res != None:
        res = spider.process()
        print("result=%s" % (res,))
    spider.close()

if __name__ == '__main__':
    main()
