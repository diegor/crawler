#!/usr/bin/python3


# TODO: that's a kludge!
import sys
sys.path.append('..')


import pprint
pp = pprint.PrettyPrinter()

import functools
from bottle import route, get, run, jinja2_view, template, request, response, static_file

from jinja2 import Template
import jinja2

import collections
import logging
import os
import threading
import time

import crawler

FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(filename='test.log', level=logging.DEBUG, format=FORMAT)
# logging.Formatter(fmt='%(asctime)s.%(msecs)03d',datefmt='%Y-%m-%d,%H:%M:%S')

# view = functools.partial(jinja2_view, template_lookup=['templates'])

env = jinja2.Environment(
    loader=jinja2.FileSystemLoader('.'),
    autoescape=True,
)

# Assuming env has already been defined in the module's scope.


def j2view(template_name):
    """ decorator for use template in jinja2 """
    def decorator(view_func):
        @functools.wraps(view_func)
        def wrapper(*args, **kwargs):
            response = view_func(*args, **kwargs)

            if isinstance(response, dict):
                template = env.get_or_select_template(template_name)
                return template.render(**response)
            else:
                return response

        return wrapper

    return decorator


@route('/hello/<name>')
def index(name):
    return Template('<b>Hello {{name}}</b>!').render(name=name)


@route('/form')
@j2view('form.html')
def form():
    print(request)
    if request.query.name != '':
        response.set_cookie('name', request.query.name)
    return {'name': request.query.name}


@route('/cookies')
@j2view('cookies.html')
def form():
    pp.pprint(request.cookies)
    # salva il nome tra i cookie
    name = "a stranger"
    if request.cookies.name:
        name = request.cookies.name
    return {'name': name}


@route('/crawler/addurl')
@j2view('crawler_addurl.html')
def crawler_form():
    fields = ('url', 'recurse', 'reclevel', 'same_domain')
    print("request.params:")
    for k in request.params:
        pp.pprint((k, request.params[k]))
    if 'url' in request.params:
        url = request.params['url']
    else:
        url = None

    #    spider.add(DownloadRequest("https://www.trueelena.org", reclevel=1))
    r = {}
    for f in fields:
        if f in request.params:
            r[f] = request.params[f]
        else:
            r[f] = None
    if url != None:
        dr = crawler.DownloadRequest(**r)
        spider.add(dr)
        r['cache_url'] = spider.url_converter(url)

    logging.info("Add " + repr(r))

    return r


@route('/crawler/status/cache')
def crawler_status_cache():
    pass


@route('/crawler/status/queue')
@j2view('status.html')
def crawler_status_queue():
    return {'count': len(list(spider.queue())), 'list': list(spider.queue())}


@get('/cache/<filepath:path>')
def static_cache(filepath):
    logging.debug("read %s" % filepath)
    print("read %s" % filepath)
    # TODO: check if it is a directory and add "/index.html"
    if os.path.isdir("/tmp/cache/" + filepath):
        filepath = filepath + "/index.html"
    return static_file(filepath, root="/tmp/cache/")

spider = crawler.WebCrawler()
spider.configure(path="/tmp/cache", cache_url="https://51.15.73.56/cache/")


print("Starting worker thread")


def worker():
    res = 0

    while True:
        try:
            res = spider.process()
        except Exception as e:
            res = e
        print("result=%s" % (res,))
        time.sleep(3)

w = threading.Thread(name='worker', target=worker)
w.start()

print("Starting web server")

# run(host='10.9.132.67', port=8080)
run(host='127.0.0.1', port=8080)
