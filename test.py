import unittest
import os
import shutil

from crawler import WebCrawler, DownloadRequest


class WebCrawlerTest(unittest.TestCase):

    def setUp(self):
        self.dirStore = '/tmp/test'
        self.wc = WebCrawler()
        if not os.path.isdir(self.dirStore):
            os.makedirs(self.dirStore)
        self.wc.configure(path=self.dirStore)

    def tearDown(self):
        shutil.rmtree(self.dirStore)
        self.wc.close()

    def test_process_null(self):
        wc = WebCrawler()
        self.assertTrue(
            wc.is_empty(), "new WebCrowler instance should be is_empty() == True")
        self.assertEqual(
            wc.process(), None, 'process() must return None if there is no DownloadRequest')

    def test_process_1(self):
        self.wc.add(DownloadRequest(url='http://www.trueelena.org'))
        print("self.wc.dq=%s" % (self.wc.dq.queue,))
        self.assertFalse(self.wc.is_empty())
        url, res = self.wc.process()
        self.assertNotEqual(res, None)

    def test_visited(self):
        self.wc.add(DownloadRequest(url='http://www.trueelena.org'))
        url, res1 = self.wc.process()
        self.assertEqual(len(self.wc.dq.processed()), 1, "crawler.process should process a url at a time")
        self.assertEqual(self.wc.dq.processed()[url], res1, "processed must return a dict, with the processed request")


if __name__ == "__main__":
    unittest.main()
